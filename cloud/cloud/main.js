var twilio = require('twilio')('ACf309377567bac5d9316a8f5ed7149e16', '48059e872de12c5fd8d90d5e8e2f31d4');
var serialize = function(obj) { var str = []; for(var p in obj) if (obj.hasOwnProperty(p)) { str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])); } return str.join("&"); }

Parse.Cloud.define("sendVerificationCode", function(request, response) {
    var verificationCode = Math.floor(Math.random()*999999);
    var user = Parse.User.current();
    user.set("phoneVerificationCode", verificationCode);
    user.save();
    
    twilio.sendSms({
        From: "510-257-9109",
        To: request.params.phoneNumber,
        Body: "Your verification code is " + verificationCode + "."
    }, function(err, responseData) { 
        if (err) {
          response.error(err);
        } else { 
          response.success("Success");
        }
    });
});

Parse.Cloud.define("verifyPhoneNumber", function(request, response) {
    var user = Parse.User.current();
    var verificationCode = user.get("phoneVerificationCode");
    if (verificationCode == request.params.phoneVerificationCode) {
        user.set("phoneNumber", request.params.phoneNumber);
        user.save();
        response.success("Success");
    } else {
        response.error("Invalid verification code.");
    }
});

Parse.Cloud.define("sendLocation", function(request, response) {
    var myPhoneNumber = request.params.myPhoneNumber;
    var numberToSend = request.params.numberToSend;
    var map = request.params.map;
    var address = request.params.address;

    var params = serialize({
        To: numberToSend,
        From: '+15102579109',
        Body: myPhoneNumber + ' is approximiately at: ' + address,
        MediaUrl: map
    });

    twilio.request({
        method: 'POST',
        url: '/Accounts/ACf309377567bac5d9316a8f5ed7149e16/Messages',
        body: params
    }, function (err, message) {
        if (err) {
            response.error(err);
        } else {
            response.success("Success");
        }
    });
});