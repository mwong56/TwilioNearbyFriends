package nearbyfriends.mwong56.com.nearbyfriends.service;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import nearbyfriends.mwong56.com.nearbyfriends.location.TimeoutFusedLocationListener;
import nearbyfriends.mwong56.com.nearbyfriends.location.TimeoutLocationListener;
import nearbyfriends.mwong56.com.nearbyfriends.receiver.IncomingSmsReceiver;
import nearbyfriends.mwong56.com.nearbyfriends.util.CommonUtil;

public class LocationService extends Service implements TimeoutFusedLocationListener.TimeoutFusedLocationListenerInterface, TimeoutLocationListener.TimeoutLocationListenerInterface {

  private static final String TAG = LocationService.class.getSimpleName();
  private static final int LOCATION_INTERVAL = 1000;
  private static final float LOCATION_DISTANCE = 10f;

  private LocationManager locationManager = null;
  private GoogleApiClient client;
  private String numberToSend;
  private String myPhoneNumber;
  private TimeoutFusedLocationListener fusedLocationListener;
  private TimeoutLocationListener locationlistener;

  private LocationService that;

  @Override
  public IBinder onBind(Intent arg0) {
    return null;
  }


  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    super.onStartCommand(intent, flags, startId);
    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    manager.cancel(intent.getExtras().getInt(IncomingSmsReceiver.NOTIFICATION_ID, -1));

    numberToSend = intent.getExtras().getString("numberToSend");
    TelephonyManager tMgr = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
    myPhoneNumber = tMgr.getLine1Number();

    that = this;
    client = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
          @Override
          public void onConnected(Bundle bundle) {
            if (CommonUtil.isLocationEnabled(getApplicationContext())) {
              LocationRequest locationRequest = new LocationRequest();
              locationRequest.setInterval(10000);
              locationRequest.setFastestInterval(5000);
              locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
              fusedLocationListener = new TimeoutFusedLocationListener(client, 10000, that);
              LocationServices.FusedLocationApi.requestLocationUpdates(
                  client, locationRequest, fusedLocationListener);
            } else {
              getLastFusedGps();
            }
          }

          @Override
          public void onConnectionSuspended(int i) {
          }
        })
        .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
          @Override
          public void onConnectionFailed(ConnectionResult connectionResult) {
            getGpsWithoutGPS();
          }
        })
        .addApi(LocationServices.API)
        .build();

    client.connect();

    return START_STICKY;
  }

  private void getGpsWithoutGPS() {
    initializeLocationManager();
    if (CommonUtil.isLocationEnabled(getApplicationContext())) {
      getGpsLocation();
    } else {
      getLastBestLocation(0);
    }
  }

  private void getLastFusedGps() {
    Location location = LocationServices.FusedLocationApi.getLastLocation(client);
    if (location != null) {
      postLocation(location);
      shutdown();
    } else {
      getGpsWithoutGPS();
    }
  }

  public void getLastBestLocation(long minTime) {
    Location bestResult = null;
    float bestAccuracy = Float.MAX_VALUE;
    long bestTime = Long.MIN_VALUE;

    // Iterate through all the providers on the system, keeping
    // note of the most accurate result within the acceptable time limit.
    // If no result is found within maxTime, return the newest Location.
    List<String> matchingProviders = locationManager.getAllProviders();
    for (String provider : matchingProviders) {
      Location location = locationManager.getLastKnownLocation(provider);
      if (location != null) {
        float accuracy = location.getAccuracy();
        long time = location.getTime();

        if ((time > minTime && accuracy < bestAccuracy)) {
          bestResult = location;
          bestAccuracy = accuracy;
          bestTime = time;
        } else if (time < minTime && bestAccuracy == Float.MAX_VALUE && time > bestTime) {
          bestResult = location;
          bestTime = time;
        }
      }
    }

    if (bestResult != null) {
      postLocation(bestResult);
    } else {
      Toast.makeText(getBaseContext(), "Location services not on.", Toast.LENGTH_SHORT).show();
    }
  }

  private void getGpsLocation() {
    locationlistener = new TimeoutLocationListener(locationManager, 10000, this);
    try {
      locationManager.requestLocationUpdates(
          LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, locationlistener);
    } catch (java.lang.SecurityException ex) {
      Log.i(TAG, "fail to request location update, ignore", ex);
    } catch (IllegalArgumentException ex) {
      Log.d(TAG, "gps provider does not exist " + ex.getMessage());
    }
  }

  public static String getGoogleMapUrl(double lati, double longi) {
    String URL = "http://maps.google.com/maps/api/staticmap?center=" + lati + "," + longi
        + "&zoom=14&size=200x200&sensor=false&markers=color:red%7C" + lati + "," + longi;
    return URL;
  }

  private void postLocation(Location location) {
    Map<String, String> map = new HashMap<String, String>();
    map.put("numberToSend", numberToSend);
    map.put("myPhoneNumber", myPhoneNumber);
    map.put("map", getGoogleMapUrl(location.getLatitude(), location.getLongitude()));

    Geocoder geocoder;
    List<Address> addresses = null;
    geocoder = new Geocoder(this, Locale.getDefault());

    try {
      addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
    } catch (Exception e) {

    }

    if (addresses != null) {
      String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
      String city = addresses.get(0).getLocality();
      String state = addresses.get(0).getAdminArea();
      String country = addresses.get(0).getCountryName();
      String postalCode = addresses.get(0).getPostalCode();
      String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
      map.put("address", address + " " + city + ", " + state + ", " + postalCode);
    }

    ParseCloud.callFunctionInBackground("sendLocation", map, new FunctionCallback<String>() {
      @Override
      public void done(String s, ParseException e) {
        Toast.makeText(getApplicationContext(), "Location sent!", Toast.LENGTH_SHORT).show();
      }
    });
    removeLocationUpdates();
    stopSelf();
  }


  @Override
  public void onDestroy() {
    Log.e(TAG, "onDestroy");
    super.onDestroy();
    shutdown();
  }

  private void removeLocationUpdates() {
    if (locationManager != null) {
      locationManager.removeUpdates(locationlistener);
    }
    if (fusedLocationListener != null) {
      LocationServices.FusedLocationApi.removeLocationUpdates(client, fusedLocationListener);
    }
  }

  private void initializeLocationManager() {
    Log.e(TAG, "initializeLocationManager");
    if (locationManager == null) {
      locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
    }
  }

  private void shutdown() {
    removeLocationUpdates();
    stopSelf();
  }

  @Override
  public void onTimeouted(android.location.LocationListener sender) {
    getLastBestLocation(0);
  }

  @Override
  public void onFusedTimeouted() {
    getLastFusedGps();
  }

  @Override
  public void onLocationChanged(Location location) {
    Log.e(TAG, "onLocationChanged: " + location);
    postLocation(location);
    shutdown();
  }
}