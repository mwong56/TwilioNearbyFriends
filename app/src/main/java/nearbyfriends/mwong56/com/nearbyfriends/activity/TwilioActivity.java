package nearbyfriends.mwong56.com.nearbyfriends.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import java.util.HashMap;
import java.util.Map;

import nearbyfriends.mwong56.com.nearbyfriends.R;


public class TwilioActivity extends ActionBarActivity {

    private EditText phoneNumber;
    private EditText verificationCode;

    private static final String TAG = TwilioActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_twilio);

        phoneNumber = (EditText) findViewById(R.id.phone_number);
        phoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        verificationCode = (EditText) findViewById(R.id.verification_code);

        phoneNumber.setVisibility(View.VISIBLE);
        verificationCode.setVisibility(View.GONE);

        phoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    sendVerification(phoneNumber.getText().toString());
                }
                return false;
            }
        });

        verificationCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    verifyPhoneNumber(verificationCode.getText().toString());
                }
                return false;
            }
        });

    }

    private void sendVerification(String number) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("phoneNumber", number);
        ParseCloud.callFunctionInBackground("sendVerificationCode", map);
        phoneNumber.setVisibility(View.GONE);
        verificationCode.setVisibility(View.VISIBLE);
    }

    private void verifyPhoneNumber(String code) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("phoneVerificationCode", code);

        ParseCloud.callFunctionInBackground("verifyPhoneNumber", map, new FunctionCallback<String>() {
            @Override
            public void done(String response, ParseException e) {
                if (e == null && response.equals("Success")) {
                    updateParseUserInfoInBackground();
                }
            }
        });
    }

    private void updateParseUserInfoInBackground() {

    }
}
