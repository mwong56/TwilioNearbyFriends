package nearbyfriends.mwong56.com.nearbyfriends.location;

import android.location.Location;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;

import java.util.Timer;
import java.util.TimerTask;

/**
 * TimeoutableLocationListner is implementation of LocationListener.
 * If onLocationChanged isn't called within XX mili seconds, automatically remove listener.
 *
 * @author amay077 - http://twitter.com/amay077
 */
public class TimeoutFusedLocationListener implements LocationListener {
  protected Timer timerTimeout = new Timer();
  private TimeoutFusedLocationListenerInterface listener;
  private GoogleApiClient client;

  public TimeoutFusedLocationListener(GoogleApiClient client, long timeOutMS,
                                      final TimeoutFusedLocationListenerInterface timeoutListener) {
    this.client = client;
    this.listener = timeoutListener;
    timerTimeout.schedule(new TimerTask() {

      @Override
      public void run() {
        if (listener != null) {
          listener.onFusedTimeouted();
        }
        stopLocationUpdateAndTimer();
      }
    }, timeOutMS);
  }

  /**
   * Location callback.
   * <p/>
   * If override on your concrete class, must call base.onLocation().
   */
  @Override
  public void onLocationChanged(Location location) {
    stopLocationUpdateAndTimer();
    listener.onLocationChanged(location);
  }

  private void stopLocationUpdateAndTimer() {
    LocationServices.FusedLocationApi.removeLocationUpdates(client, this);

    timerTimeout.cancel();
    timerTimeout.purge();
    timerTimeout = null;
  }

  public interface TimeoutFusedLocationListenerInterface {
    void onLocationChanged(Location location);
    void onFusedTimeouted();
  }
}