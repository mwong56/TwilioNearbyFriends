package nearbyfriends.mwong56.com.nearbyfriends.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.Random;

import nearbyfriends.mwong56.com.nearbyfriends.service.LocationService;
import nearbyfriends.mwong56.com.nearbyfriends.R;
import nearbyfriends.mwong56.com.nearbyfriends.activity.DismissNotificationActivity;
import nearbyfriends.mwong56.com.nearbyfriends.util.CommonUtil;

public class IncomingSmsReceiver extends BroadcastReceiver {

    public static final String NOTIFICATION_ID = "NOTIFICATION_ID";
    private final static String MESSAGE_KEY = "!";
    private final static String PROTOCOL_DESCRIPTION_UNIT = "pdus";
    private static final int PRIORITY_HIGH = 5;
    private static final String TAG = IncomingSmsReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();

        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get(PROTOCOL_DESCRIPTION_UNIT);

                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();
                    Log.i("SmsReceiver", "senderNum: " + phoneNumber + "; message: " + message);
                    message = message.trim();

                    if (message.equals(MESSAGE_KEY)) {
                        int notificationId = new Random().nextInt(); // just use a counter in some util class...
                        PendingIntent dismissIntent = DismissNotificationActivity.getDismissIntent(notificationId, context);

                        Intent service = new Intent(context, LocationService.class);
                        service.putExtra("numberToSend", phoneNumber);
                        service.putExtra(NOTIFICATION_ID, notificationId);
                        PendingIntent pIntent = PendingIntent.getService(context, 0, service, PendingIntent.FLAG_CANCEL_CURRENT);

                        Notification n = new Notification.Builder(context)
                            .setContentTitle("Location request")
                            .setContentText("Share your location to " + CommonUtil.formatPhoneNumber(phoneNumber, context.getApplicationContext()) + "?")
                            .setSmallIcon(android.R.drawable.ic_menu_myplaces)
                            .setAutoCancel(true)
                            .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS)
                            .setPriority(PRIORITY_HIGH)
                            .addAction(R.drawable.ic_clear_black_24dp, "IGNORE", dismissIntent)
                            .addAction(R.drawable.ic_done_black_24dp, "ACCEPT", pIntent)
                            .build();

                        NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(notificationId, n);
                    }
                }
            }

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }
}