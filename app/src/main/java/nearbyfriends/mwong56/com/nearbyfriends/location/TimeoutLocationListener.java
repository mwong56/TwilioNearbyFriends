package nearbyfriends.mwong56.com.nearbyfriends.location;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import java.util.Timer;
import java.util.TimerTask;

/**
 * TimeoutableLocationListner is implementation of LocationListener.
 * If onLocationChanged isn't called within XX mili seconds, automatically remove listener.
 *
 * @author amay077 - http://twitter.com/amay077
 */
public class TimeoutLocationListener implements LocationListener {
  protected Timer timerTimeout = new Timer();
  protected LocationManager locaMan = null;
  private TimeoutLocationListenerInterface timeoutTableListener;

  /**
   * Initialize instance.
   *
   * @param locaMan         the base of LocationManager, can't set null.
   * @param timeOutMS       timeout elapsed (mili seconds)
   * @param timeoutListener if timeout, call onTimeouted method of this.
   */
  public TimeoutLocationListener(LocationManager locaMan, long timeOutMS,
                                 final TimeoutLocationListenerInterface timeoutListener) {
    this.locaMan = locaMan;
    this.timeoutTableListener = timeoutListener;
    timerTimeout.schedule(new TimerTask() {

      @Override
      public void run() {
        if (timeoutListener != null) {
          timeoutListener.onTimeouted(TimeoutLocationListener.this);
        }
        stopLocationUpdateAndTimer();
      }
    }, timeOutMS);
  }

  /**
   * Location callback.
   * <p/>
   * If override on your concrete class, must call base.onLocation().
   */
  @Override
  public void onLocationChanged(Location location) {
    stopLocationUpdateAndTimer();
    timeoutTableListener.onLocationChanged(location);
  }

  @Override
  public void onProviderDisabled(String s) {
  }

  @Override
  public void onProviderEnabled(String s) {
  }

  @Override
  public void onStatusChanged(String s, int i, Bundle bundle) {
  }

  private void stopLocationUpdateAndTimer() {
    locaMan.removeUpdates(this);

    timerTimeout.cancel();
    timerTimeout.purge();
    timerTimeout = null;
  }

  public interface TimeoutLocationListenerInterface {
    void onLocationChanged(Location location);
    void onTimeouted(LocationListener sender);
  }
}