package nearbyfriends.mwong56.com.nearbyfriends.activity;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import nearbyfriends.mwong56.com.nearbyfriends.receiver.IncomingSmsReceiver;

/**
 * Created by mwong on 6/18/15.
 */
public class DismissNotificationActivity extends Activity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    manager.cancel(getIntent().getIntExtra(IncomingSmsReceiver.NOTIFICATION_ID, -1));
    finish(); // since finish() is called in onCreate(), onDestroy() will be called immediately
  }

  public static PendingIntent getDismissIntent(int notificationId, Context context) {
    Intent intent = new Intent(context, DismissNotificationActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    intent.putExtra(IncomingSmsReceiver.NOTIFICATION_ID, notificationId);
    PendingIntent dismissIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    return dismissIntent;
  }

}